﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {

    public bool isGround = false;

    Rigidbody2D rigid;
    BoxCollider2D boxCol;

    public float velocity = 40f;
    void Start() {
        rigid = GetComponent<Rigidbody2D>();
        boxCol = GetComponent<BoxCollider2D>();
    }

    void Update() {
        if (isGround) {
            rigid.AddForce(Vector2.up * velocity);
        }

        if (rigid.velocity.y > 0) {
            boxCol.isTrigger = true;
        }
        else {
            boxCol.isTrigger = false;
        }
    }

    public void OnCollisionStay2D(Collision2D collision) {
        if (collision.transform.CompareTag("Ground")) {
            isGround = true;

        }
    }

   
}
